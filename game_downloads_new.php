<?php
require_once("php/inc.functions.php");
require_once("php/plugin.game_info.php");
require_once("php/plugin.game_req_on_team.php");


if (isset($_POST["submit"])) {
  if ($_FILES["file"]["error"] > 0 && $_POST["url"] == "") {
    die("Upload Error Code: " . $_FILES["file"]["error"]);
  } else {
    $query = $db->prepare("INSERT INTO game_downloads (game_id, filename, url, platform, description, size, upload_time) VALUES
      (:game_id, :filename, :url, :platform, :description, :size, FROM_UNIXTIME(".time()."))"); 
    if ($_FILES["file"]["name"] != "") {
      // The user provided a file to upload.
      $query->execute(array(
        ":game_id" => $game_id,
        ":filename" => $_FILES["file"]["name"],
        ":url" => "",
        ":platform" => $_POST["platform"],
        ":description" => $_POST["description"],
        ":size" => $_FILES["file"]["size"]
      ));

      $query = $db->prepare("SELECT id FROM game_downloads WHERE filename=:filename AND game_id=:game_id AND size=:size ORDER BY id DESC LIMIT 1"); 
      $query->execute(array(
        ":game_id" => $game_id,
        ":filename" => $_FILES["file"]["name"],
        ":size" => $_FILES["file"]["size"]
      ));
      $dl_id = $query->fetchAll(PDO::FETCH_ASSOC);
      $dl_id = $dl_id[0]["id"];

      $final_file_name  = "games/".$game_info["uid"]."/downloads/".$_FILES["file"]["name"];
      move_uploaded_file($_FILES["file"]["tmp_name"], $final_file_name);
      $link = "<a href=\"".$final_file_name."\">".$_FILES["file"]["name"]."</a>";
    } else {
      // the user provided a url
      $query->execute(array(
        ":game_id" => $game_id,
        ":filename" => "",
        ":url" => $_POST["url"],
        ":platform" => $_POST["platform"],
        ":description" => $_POST["description"],
        ":size" => 0
      ));

      $link = end(explode($_POST["url"]));
      $link = "<a href=\"".$_POST["url"]."\">".$link."</a>";
    }

    $news_platform = "";

    if (intval($_POST["platform"]) != -1) {
      $query = $db->prepare("SELECT name FROM platforms WHERE id=:id LIMIT 1");
      $query->execute(array(
        ":id" => $_POST["platform"]
      ));
      $platform_info = $query->fetchAll(PDO::FETCH_ASSOC);
      if (count($platform_info) > 0) {
        $news_platform = " for ".$platform_info[0]["name"];
      }
    }

    $query = $db->prepare("INSERT INTO game_updates (game_id, pid, message, major, date, edit_date)
      VALUES (:game_id, :pid, :message, :major, FROM_UNIXTIME(".time()."), FROM_UNIXTIME(".time()."))");
    $query->execute(array(
      ":game_id" => $game_id,
      ":pid" => $_SESSION["user"]->username,
      ":message" => "A new binary, ".$link.$news_platform.", was uploaded.",
      ":major" => 0
    ));
    header("Location: game_project.php?game=".$_GET["game"]);
    exit;
  }
}


?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>New Download :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>

      <!-- Content -->
      <h1>
        New Download <a href="game_project.php?game=<?=$game ?>"><small ><font class="hide-for-small"><?=$game_info["name"] ?></font></small></a>
      </h1>
      <hr />
      <div class="row">
        <div class="twelve columns" style="margin-bottom:30px;">
          <form action="game_downloads_new.php?game=<?=$_GET["game"] ?>" method="post" enctype="multipart/form-data">
          <h5 class="subheader" style="font-weight:bold;">
            Upload a file: <input type="file" name="file" id="file"><br>
            or enter an external URL:<input type="text" name="url"><br>
            Description: <small style="color:#fff;">(optional)</small><input type="text" name="description"><br>
            Platform:
            <select name="platform">
              <option value="-1">N/A (or more than one)</option>
              <?php
              $query = $db->prepare("SELECT id, name, uid, url FROM  `platforms`"); 
              $query->execute(array());
              $platforms = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i = 0; $i < count($platforms); $i++) {
                ?>
                <option value="<?=$platforms[$i]["id"] ?>" > <?=$platforms[$i]["name"] ?></option>
                <?php
              }
              ?>
            </select>
            <br>
            <br>
            <input type="submit" name="submit" value="Upload Binary" class="nice blue radius button" style="color:#333;">
          </h5>
          </form>
        </div>
      </div>
      <!-- End Content -->

      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
