<?php
require_once("php/inc.functions.php");

$query = $db->prepare("SELECT * FROM games");
$query->execute();
$games = $query->fetchAll(PDO::FETCH_ASSOC);


?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Games :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
      <h1>
        Games 
        <small><font class="hide-for-small">What we're working on</font></small>
        <?php if (isset($_SESSION["user"])) { ?><a href="game_new.php"><small style="float:right; margin-top:32px;">Add New Game</small></a><?php } ?>
      </h1>
      <hr>

      <!-- Thumbnails -->
      <div class="row">
        <div class="twelve show-for-small columns">
          <h3>Recent Projects</h3>
        </div>
        <?php
        for ($i = 0; $i < count($games); $i++) {
          $game_info = getGameInfo($games[$i]["id"]);
          square_game($games[$i]["name"], $games[$i]["uid"], false, $game_info["last_update"]);
        }
        for ($i = 0; $i < 4-(count($games)%4) && count($games) % 4 != 0; $i++) {
          square_game();
        }
        ?>
      </div>
      <!-- End Thumbnails -->


      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
