<?php
require_once("php/inc.functions.php");

?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Social :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "social"; include("php/inc.nav-bar.php"); ?>
      <div class="row">
        <div class="three hide-for-small columns"></div>
        <a href="https://www.facebook.com/pages/Virginia-Tech-Gaming-Project-VTGP/335125083201062">
        <div class="three mobile-two columns">
          <img src="img/facebook_vtgp.png">
        </div>
        </a>
        <a href="https://twitter.com/GamingProject">
          <div class="three mobile-two columns">
            <img src="img/twitter_vtgp.png">
          </div>
        </a>
        <div class="three hide-for-small columns"></div>
      </div>


      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
