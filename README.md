# VT Gaming Project Website

This is the official repo of the VT Gaming Project's website.  The site was designed as a full-feature CMS to allow members of the club a place to share the work they were doing on their games.

You can see this site in production at [www.vtgamingproject.org.vt.edu](http://www.vtgamingproject.org.vt.edu/).

![vtgp-site-home.png](https://bitbucket.org/repo/kKk7qr/images/23864872-vtgp-site-home.png)