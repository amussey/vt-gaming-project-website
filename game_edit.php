<?php
require_once("php/inc.functions.php");
require_once("php/plugin.game_info.php");
require_once("php/plugin.game_req_on_team.php");

if (!isset($_SESSION["user"])) {
  header("Location: games.php");
  exit;
}


$_POST["uid"] = $game_info["uid"];
if (isset($_POST["submit"])) {
  $_POST["name"] = trim($_POST["name"]);
  if (strlen($_POST["name"]) == 0) {
    $_FERROR["name"] = "Your new project must have a name.";
  }

  if (!isset($_POST["platform"])) {
    $_FERROR["platform"] = "You must select at least one target platform.";
  }

  if (!isset($_FERROR)){
    $_POST["platform"] = implode(";", $_POST["platform"]);

    $query = $db->prepare("UPDATE games
      SET name=:name, keywords=:keywords, description=:description, platforms=:platforms, repo=:repo, moddb=:moddb
      WHERE uid=:uid ");
    $query->execute(array(
      ":name" => $_POST["name"],
      ":uid" => $_POST["uid"],
      ":keywords" => "",
      ":description" => $_POST["description"],
      ":platforms" => $_POST["platform"],
      ":repo" => $_POST["repo"],
      ":moddb" => $_POST["moddb"]
    ));
    header("Location: game_project.php?game=".$game_info["uid"]);
    exit;
  }
}

$_POST["name"] = $game_info["name"];
$_POST["description"] = $game_info["description"];
$_POST["repo"] = $game_info["repo"];
$_POST["moddb"] = $game_info["moddb"];


?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Edit Game :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>

      <h1>
        Games <small ><font class="hide-for-small">New Game</font></small>
      </h1>
      <hr />

      <div class="row">
        <div class="panel" style="min-height:440px; background-color:transparent; border-color:transparent;">
          <form method="POST" action="game_edit.php?game=<?=$_GET["game"] ?>">
            <h5 class="subheader">
              <b style="<?=(isset($_FERROR["name"]) ? "color:#F00;" : "") ?>">Project Name<?=(isset($_FERROR["name"]) ? " <small style=\"color:#f00; font-weight:bold;\">".$_FERROR["name"]."</small>" : "") ?></b>:
              <input type="text" name="name" value="<?=(isset($_POST["name"]) ? $_POST["name"] : "") ?>"><br>

              <b>Description</b>:<textarea name="description" rows="5"><?=(isset($_POST["description"]) ? stripslashes ($_POST["description"]) : "") ?></textarea>
              <br>
              <b style="<?=(isset($_FERROR["platform"]) ? "color:#F00;" : "") ?>">Platforms</b> <small style="color:#f00; font-weight:bold;"><?=(isset($_FERROR["platform"]) ? $_FERROR["platform"] : "") ?></small>:
              <br>
              <?php
              $query = $db->prepare("SELECT id, name, uid, url FROM  `platforms`");
              $query->execute(array());
              $platforms = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i = 0; $i < count($platforms); $i++) { ?>
                <label style="color:#fff;"><input type="checkbox" name="platform[]" value="<?=$platforms[$i]["id"] ?>" <?php
                for ($j = 0; $j < count($game_info["platforms"]); $j++) {
                  if ($game_info["platforms"][$j]["id"] == $platforms[$i]["id"]) {
                    echo "checked";
                  }
                }
                ?> /> <?=$platforms[$i]["name"] ?><sup>[<a href="<?=$platforms[$i]["url"] ?>" target="_blank">?</a>]</sup></label>
                <?php
              }
              ?><br>
              <b>Source Repository</b>
              <small style="color:#fff;">(<a href="http://bitbucket.org">BitBucket provides free git hosting with your @vt.edu email</a>)</small>:
              <input type="text" name="repo" value="<?=(isset($_POST["repo"]) ? $_POST["repo"] : "") ?>"><br>
              <b>ModDB Page</b>:  <input type="text" name="moddb" value="<?=(isset($_POST["moddb"]) ? $_POST["moddb"] : "") ?>"><br>
              <br>
              <input type="submit" name="submit" value="Update Game Info" class="nice blue radius button" style="color:#333;">
            </h5>
          </form>
        </div>
      </div>
      <br>

      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
