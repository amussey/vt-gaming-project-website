<?php
require_once("php/inc.functions.php");


if (!isset($_SESSION["user"])) {
  header("Location: games.php");
  exit;
}


if (isset($_POST["submit"])) {
  $_POST["uid"] = strtolower($_POST["uid"]);
  $_POST["name"] = trim($_POST["name"]);
  if (strlen($_POST["name"]) == 0) {
    $_FERROR["name"] = "Your new project must have a name.";
  }
  $_POST["uid"] = str_replace(" ", "", trim($_POST["uid"]));
  if (strlen($_POST["uid"]) == 0) {
    $_FERROR["uid"] = "Your new project must have a keyword.";
  }

  $query = $db->prepare("SELECT id FROM games WHERE uid=:uid LIMIT 1");
  $query->execute(array(
    ":uid" => $_POST["uid"]
  ));
  if (count($query->fetchAll(PDO::FETCH_ASSOC))) {
    $_FERROR["uid"] = "That ID is already in use.";
  }

  if (!isset($_POST["platform"])) {
    $_FERROR["platform"] = "You must select at least one target platform.";
  }

  if (!isset($_FERROR)) {
    mkdir("games/".$_POST["uid"]."");
    mkdir("games/".$_POST["uid"]."/downloads");
    mkdir("games/".$_POST["uid"]."/screenshots");
    chmod("games/".$_POST["uid"], "0777");
    copy("games/500px.png", "games/".$_POST["uid"]."/500px_thumb.png");
    $_POST["platform"] = implode(";", $_POST["platform"]);
    $query = $db->prepare("INSERT INTO games (name, uid, keywords, description, platforms, repo, moddb, started)
      VALUES (:name, :uid, :keywords, :description, :platforms, :repo, :moddb, FROM_UNIXTIME(".time()."))");
    $query->execute(array(
      ":name" => $_POST["name"],
      ":uid" => $_POST["uid"],
      ":keywords" => "",
      ":description" => $_POST["description"],
      ":platforms" => $_POST["platform"],
      ":repo" => $_POST["repo"],
      ":moddb" => $_POST["moddb"]
    ));
    header("Location: game_project.php?game=".$_FERROR["uid"]);
    exit;
  }
}


?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Create a Game :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
        <h1>
            Games <small ><font class="hide-for-small">New Game</font></small>
        </h1>
      <hr />

      <div class="row">
        <div class="panel" style="min-height:440px; background-color:transparent; border-color:transparent;">
          <form method="POST" action="game_new.php">
            <h5 class="subheader">
              <b style="<?=(isset($_FERROR["name"]) ? "color:#F00;" : "") ?>">Project Name<?=(isset($_FERROR["name"]) ? " <small style=\"color:#f00; font-weight:bold;\">".$_FERROR["name"]."</small>" : "") ?></b>:
              <input type="text" name="name" value="<?=(isset($_POST["name"]) ? $_POST["name"] : "") ?>"><br>
              <b style="<?=(isset($_FERROR["uid"]) ? "color:#F00;" : "") ?>">Single Word Keyword</b> <small style="color:#fff;">(ex, "Meteor Mayhem" is "meteor".  THIS CANNOT BE CHANGED ONCE SET.)</small> <?=(isset($_FERROR["uid"]) ? " <small style=\"color:#f00; font-weight:bold;\">".$_FERROR["uid"]."</small>" : "") ?>:<input type="text" name="uid" value="<?=(isset($_POST["uid"]) ? $_POST["uid"] : "") ?>"><br>
              <b >Description</b>:<textarea name="description" rows="5"><?=(isset($_POST["description"]) ? $_POST["description"] : "") ?></textarea>
              <br>
              <b style="<?=(isset($_FERROR["platform"]) ? "color:#F00;" : "") ?>">Platforms</b> <small style="color:#f00; font-weight:bold;"><?=(isset($_FERROR["platform"]) ? $_FERROR["platform"] : "") ?></small>: <br>

              <?php
              $query = $db->prepare("SELECT id, name, uid, url FROM `platforms`");
              $query->execute(array());
              $platforms = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i = 0; $i < count($platforms); $i++) { ?>
                  <label style="color:#fff;"><input type="checkbox" name="platform[]" value="<?=$platforms[$i]["id"] ?>" /> <?=$platforms[$i]["name"] ?><sup>[<a href="<?=$platforms[$i]["url"] ?>" target="_blank">?</a>]</sup></label>
              <?php } ?>
              <br>
              <b>Source Repository</b>
              <small style="color:#fff;">(<a href="http://bitbucket.org">BitBucket provides free git hosting with your @vt.edu email</a>)</small>:
              <input type="text" name="repo" value="<?=(isset($_POST["repo"]) ? $_POST["repo"] : "") ?>"><br>
              <b>ModDB Page</b>: <input type="text" name="moddb" value="<?=(isset($_POST["moddb"]) ? $_POST["moddb"] : "") ?>"><br>
              <br>
              <input type="submit" name="submit" value="Create New Game" class="nice blue radius button" style="color:#333;">
            </h5>
          </form>
        </div>
      </div>
      <br>

      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
