<?php
require_once("php/inc.mysql.php");
require_once("php/inc.functions.php");

$query = $db->prepare("SELECT * FROM games");
$query->execute();
$games = $query->fetchAll(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <?php $page="index"; include("php/inc.nav-bar.php"); ?>
  <div class="row">
    <div class="twelve columns">
      <div class="row">
        <div class="six columns">
          <img src="img/vtgp_gamecontroller.png"><br>
        </div>
        <div class="six columns">
          <div class="panel show-for-small" >
            <h5 class="subheader">
              Welcome to the home of the Virginia Tech Gaming Project! We are VTâs largest video game development group and one of the largest organizers of LAN parties on campus! Whether you have an interest in drawing, graphics, programming, or development, the club has something for you. We invite students and faculty of all ages, backgrounds, and skill levels to come by our meetings and dive into their creativity!
              <br>
              <br>
              Our meetings will be held in Lavery Hall (Turner) room 335 at 6 PM every Thursday.
              <br>
              <br>
              Join us in discussing our projects over on our <a href="https://www.facebook.com/groups/675861402438724">Facebook Group</a>!
            </h5>
          </div>
          <div class="panel hide-for-small" style="height:440px; background-color:transparent; border-color:transparent;">
            <h5 class="subheader">
              Welcome to the home of the Virginia Tech Gaming Project! We are VT's largest video game development group and one of the largest organizers of LAN parties on campus! Whether you have an interest in drawing, graphics, programming, or development, the club has something for you. We invite students and faculty of all ages, backgrounds, and skill levels to come by our meetings and dive into their creativity!
              <br>
              <br>
              Our meetings will be held in Lavery Hall (Turner) room 335 at 6 PM every Thursday.
              <br>
              <br>
              Join us in discussing our projects over on our <a href="https://www.facebook.com/groups/675861402438724">Facebook Group</a>!
            </h5>
          </div>
        </div>
      </div>

      <!-- Thumbnails -->
      <div class="row">
        <div class="twelve show-for-small columns">
          <h3>Recent Projects</h3>
          <hr>
        </div>
        <?php
        for ($i = 0; $i < count($games); $i++) {
          $game_info = getGameInfo($games[$i]["id"]);
          square_game($games[$i]["name"], $games[$i]["uid"], false, $game_info["last_update"]); //"Meteor Mayhem", "meteor", false) //2013);
        }
        for ($i = 0; $i < 4-(count($games)%4) && count($games) % 4 != 0; $i++) {
          square_game();
        }
        ?>
      </div>
      <!-- End Thumbnails -->


      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
