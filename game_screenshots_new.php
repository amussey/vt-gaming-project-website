<?php
//game_project.php
require_once("php/inc.functions.php");
require_once("php/class.ImageManipulator.php");
require_once("php/plugin.game_info.php");
require_once("php/plugin.game_req_on_team.php");


if (isset($_POST["submit"])) {
  $allowedExts = array("gif", "jpeg", "jpg", "png");
  $extension = strtolower(end(explode(".", $_FILES["file"]["name"])));
  if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/png")) && in_array($extension, $allowedExts)) {
    if ($_FILES["file"]["error"] > 0) {
      die("File upload failed.  Return Code: " . $_FILES["file"]["error"]);
    } else {
      $query = $db->prepare("INSERT INTO game_screenshots (game_id, name, description, format, version, upload) VALUES (:game_id, :name, :description, :format, :version, FROM_UNIXTIME(".time()."))");
      $query->execute(array(
        ":game_id" => $game_id,
        ":name" => $_FILES["file"]["name"],
        ":description" => $_POST["description"],
        ":format" => $extension,
        ":version" => $_POST["version"]
      ));

      $query = $db->prepare("SELECT id FROM game_screenshots WHERE name=:name AND game_id=:game_id ORDER BY id DESC LIMIT 1");
      $query->execute(array(
        ":game_id" => $game_id,
        ":name" => $_FILES["file"]["name"]
      ));
      $screenshot_id = $query->fetchAll(PDO::FETCH_ASSOC);
      $screenshot_id = $screenshot_id[0]["id"];

      $final_file_name  = "games/".$game_info["uid"]."/screenshots/".$screenshot_id.".".$extension;
      $final_thumb_name = "games/".$game_info["uid"]."/screenshots/".$screenshot_id.".thumb.jpg";
      move_uploaded_file($_FILES["file"]["tmp_name"], $final_file_name);

      $i = new ImageManipulator($final_file_name);
      $w = $i->getWidth();
      $h = $i->getHeight();
      if ($w > $h) {
        $remaining = ($w-$h)/2;
        $i->crop($remaining, 0, $remaining+$h, $h);
      } else {
        $remaining = ($h-$w)/2;
        $i->crop(0, $remaining, $w, $remaining+$w);
      }
      $i->resample(400, 400);

      $i->save($final_thumb_name);

      $query = $db->prepare("INSERT INTO game_updates (game_id, pid, message, major, date, edit_date)
        VALUES (:game_id, :pid, :message, :major, FROM_UNIXTIME(".time()."), FROM_UNIXTIME(".time()."))");
      $query->execute(array(
        ":game_id" => $game_id,
        ":pid" => $_SESSION["user"]->username,
        ":message" => "A <a href=\"game_screenshot.php?game=".$_GET["game"]."&screenshot_id=".$screenshot_id."\">new screenshot</a> was uploaded.",
        ":major" => 0
      ));

      header("Location: game_screenshots.php?game=".$_GET["game"]);
      exit;
    }
  } else {
    $imageUploadError = "Invalid file type.";
    die("Invalid image file type.");
  }
}
?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Add a New Screenshot :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
      <h1>
        New Screenshot 
        <a href="game_project.php?game=<?=$_GET["game"] ?>"><small ><font class="hide-for-small"><?=$game_info["name"] ?></font></small></a>
      </h1>
      <hr>
      <div class="row">
        <div class="twelve columns" style="margin-bottom:30px;">
          <form action="game_screenshots_new.php?game=<?=$_GET["game"] ?>" method="post" enctype="multipart/form-data">
            <h5 class="subheader">
              <b>Select an image: <input type="file" name="file" id="file"></b><br>
              <br>
              <b>Description:<input type="text" name="description" value=""></b><br>
              <b>Game Version:<input type="text" name="version" value="0.0"></b><br>

              <br>
              <input type="submit" name="submit" value="Post Screenshot" class="nice blue radius button" style="color:#333;">
            </h5>
          </form>
        </div>
      </div>


      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
