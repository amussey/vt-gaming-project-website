<?php
require_once("php/inc.functions.php");

$game = $_GET["game"];
$game_id = getGameID($game);

if ($game_id === false) {
  header("Location: games.php");
  exit;
}

$game_info = getGameInfo($game_id);
$game_info["updates"] = get_updates($game_info["id"]);

?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Game Updates :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
      <h1>
        Game Updates 
        <a href="game_project.php?game=<?=$game ?>"><small ><font class="hide-for-small"><?=$game_info["name"] ?></font></small></a>
      </h1>
      <hr>
      <div class="row">
        <div class="twelve columns">
          <?php
          if (count($game_info["updates"]) == 0) { ?>
            <center>There are currently no updates for this game.</center><?php
          } else {
            for ($i = 0; $i < count($game_info["updates"]); $i++) { ?>
              <small>
                <?php
                echo date("r", $game_info["updates"][$i]["date"]);
                if (isset($_SESSION["user"])) {
                  echo " by ".$game_info["updates"][$i]["pid"];
                }
                ?>
              </small>
              <br>
              <div style="margin-left:10px; margin-top:10px;">
                <?=$game_info["updates"][$i]["message"] ?>
              </div>
              <br>
              <?php
              if ($i+1 != count($game_info["updates"])) {
                echo "<hr />";
              }
            }
          }
          ?>
        </div>
      </div>


      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
