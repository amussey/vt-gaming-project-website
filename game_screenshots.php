<?php
require_once("php/inc.functions.php");


$game = $_GET["game"];
$game_id = getGameID($game);

if ($game_id === false) {
  header("Location: games.php");
  exit;
}

$game_info = getGameInfo($game_id);

$query = $db->prepare("
  SELECT *
  FROM  `game_screenshots`
  WHERE game_id=:game_id
  ORDER BY  `game_screenshots`.`upload` DESC");
$query->execute(array(
  ":game_id" => $game_info["id"]
));
$screenshots = $query->fetchAll(PDO::FETCH_ASSOC);


?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Game Screenshots :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
      <h1>
        Game Screenshots
        <a href="game_project.php?game=<?=$game ?>"><small ><font class="hide-for-small"><?=$game_info["name"] ?></font></small></a>
        <a href="game_screenshots_delete.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:32px;" class="hide-for-small">Delete</small></a>
        <small style="float:right; margin-top:32px;" class="hide-for-small"> / </small>
        <a href="game_screenshots_new.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:32px;" class="hide-for-small">Add</small></a>
      </h1>
      <hr>
      <div class="row">
        <?php
        for ($i = 0; $i < count($screenshots); $i++) { ?>
          <a href="game_screenshot.php?game=<?=$game_info["uid"] ?>&screenshot_id=<?=$screenshots[$i]["id"] ?>"><div class="three mobile-two columns" style="margin-bottom:30px;"><img src="games/<?=$game_info["uid"] ?>/screenshots/<?=$screenshots[$i]["id"] ?>.thumb.jpg"></div></a>
          <?php
        }
        for ($i = 0; count($screenshots) % 4 != 0 && $i < 4 - (count($screenshots) % 4); $i++) { ?>
          <div class="three mobile-two columns" style="margin-bottom:30px;"></div>
          <?php
        } ?>
      </div>


      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
