<?php
require_once("php/inc.mysql.php");
require_once("php/inc.functions.php");


// Get the name of the game, and translate that into a database ID.
$game = $_GET["game"];
$game_id = getGameID($game);

if ($game_id === false) {
  header("Location: games.php");
  exit;
}

$game_info = getGameInfo($game_id);



$screenshot_id = $_GET["screenshot_id"];

$query = $db->prepare("SELECT * FROM game_screenshots WHERE id=:screenshot_id AND game_id =:game_id ORDER BY id ASC LIMIT 1"); 
$query->execute(array(":screenshot_id" => $screenshot_id, ":game_id" => $game_id));
$current_screenshot = $query->fetchAll(PDO::FETCH_ASSOC);


// This fetches the next and previous image.
// This query is designed to allow to loop from the
// first to the last and from the last back to the first.

$query = $db->prepare("(SELECT * FROM game_screenshots WHERE id<:screenshot_id AND game_id=:game_id ORDER BY id DESC LIMIT 1)
  UNION (SELECT * FROM game_screenshots WHERE id>:screenshot_id AND game_id=:game_id ORDER BY id DESC LIMIT 1) ORDER BY id ASC LIMIT 1"); 
$query->execute(array(":screenshot_id" => $screenshot_id, ":game_id" => $game_id));
$next_screenshot = $query->fetchAll(PDO::FETCH_ASSOC);


$query = $db->prepare("(SELECT * FROM game_screenshots WHERE id>:screenshot_id AND game_id=:game_id ORDER BY id ASC LIMIT 1)
  UNION (SELECT * FROM game_screenshots WHERE id<:screenshot_id AND game_id=:game_id ORDER BY id ASC LIMIT 1)
  ORDER BY id DESC LIMIT 1 ");
$query->execute(array(":screenshot_id" => $screenshot_id, ":game_id" => $game_id));
$previous_screenshot = $query->fetchAll(PDO::FETCH_ASSOC);

$previous_screenshot = "game_screenshot.php?game=".$game."&screenshot_id=".$previous_screenshot[0]["id"];
$next_screenshot = "game_screenshot.php?game=".$game."&screenshot_id=".$next_screenshot[0]["id"];


?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Game Screenshots :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
  <script>
  function leftArrowPressed() {
    document.location.href = "<?=$previous_screenshot ?>";
  }

  function rightArrowPressed() {
    document.location.href = "<?=$next_screenshot ?>";
  }

  document.onkeydown = function(evt) {
    evt = evt || window.event;
    switch (evt.keyCode) {
      case 37:
        leftArrowPressed();
        break;
      case 39:
        rightArrowPressed();
        break;
    }
  };
  </script>
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
      <h1>
        Game Screenshots 
        <a href="game_project.php?game=<?=$game ?>"><small><font class="hide-for-small"><?=$game_info["name"] ?></font></small></a>
      </h1>
      <hr>

      <div class="row">
        <div class="two columns"><a href="<?=$previous_screenshot ?>">&lt; Previous</a></div>
        <div class="nine columns"></div>
        <div class="one columns"><a href="<?=$next_screenshot ?>">Next &gt;</a></div>
      </div>
      <br>
      <div class="row">
        <div class="twelve columns"><img src="games/<?=$game ?>/screenshots/<?=$current_screenshot[0]["id"].".".$current_screenshot[0]["format"]; ?>" width="910"></div>
      </div>
      <br>
      <div class="row">
        <div class="two columns"><a href="<?=$next_screenshot ?>">&lt; Previous</a></div>
        <div class="nine columns"></div>
        <div class="one columns"><a href="<?=$previous_screenshot ?>">Next &gt;</a></div>
      </div>


      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
