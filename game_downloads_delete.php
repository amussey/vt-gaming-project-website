<?php
require_once("php/inc.functions.php");
require_once("php/inc.mysql.php");
require_once("php/plugin.game_info.php");
require_once("php/plugin.game_req_on_team.php");


if (isset($_GET["download_id"])) {
  $query = $db->prepare("SELECT * FROM game_downloads WHERE  id=:id AND game_id=:game_id"); 
  $query->execute(array(
    ":game_id" => $game_info["id"],
    ":id" => $_GET["download_id"]
  ));
  $downloads = $query->fetchAll(PDO::FETCH_ASSOC);

  if (count($downloads) > 0) {
    if ($downloads[0]["url"] == "") {
      $final_file_name  = "games/".$game_info["uid"]."/downloads/".$downloads[0]["filename"];
      unlink($final_file_name);
    }
    $query = $db->prepare("DELETE FROM game_downloads WHERE  id=:id AND game_id=:game_id "); 
    $query->execute(array(
      ":game_id" => $game_info["id"],
      ":id" => $_GET["download_id"]
    ));
    $game_info = getGameInfo($_GET["game"]);
  }
}

?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Delete Downloads :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>

      <h1>
        Delete Downloads <small ><font class="hide-for-small">Click file to delete</font></small>
        <a href="game_project.php?game=<?=$game_info["uid"] ?>"><small style="float:right; margin-top:32px;">Done</small></a>
      </h1>
      <hr />
      <!-- End Navigation -->


      <!-- Downloads -->
      <?php
      for ($i = 0; $i < count($game_info["downloads"]); $i++) {
      ?>
        <div class="row hide-for-small">
          <div class="seven columns"><a href="game_downloads_delete.php?game=<?=$game_info["uid"] ?>&download_id=<?=$game_info["downloads"][$i]["id"] ?>"><?=$game_info["downloads"][$i]["filename"] ?></a></div>
          <div class="three columns"><?=$game_info["downloads"][$i]["platform"] ?></div>
          <div class="two columns"><?=$game_info["downloads"][$i]["size"] ?></div>
        </div>
        <br class="hide-for-small" />
      <?php
      }
      ?>
      <!-- End Downloads -->


      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>

  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
