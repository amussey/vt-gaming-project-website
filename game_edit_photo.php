<?php
require_once("php/inc.functions.php");
require_once("php/class.ImageManipulator.php");

$game = $_GET["game"];
$game_id = getGameID($game);

if ($game_id === false) {
  header("Location: games.php");
  exit;
}

$game_info = getGameInfo($game_id);


if (isset($_POST["submit"])) {
  $allowedExts = array("gif", "jpeg", "jpg", "png");
  $extension = strtolower(end(explode(".", $_FILES["file"]["name"])));
  if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/png")) && in_array($extension, $allowedExts)) {
    if ($_FILES["file"]["error"] > 0) {
      die("File failed to upload.  Return Code: " . $_FILES["file"]["error"]);
    } else {
      move_uploaded_file ($_FILES["file"]["tmp_name"], "games/".$game_info["uid"]."/500px_thumb.png");

      $i = new ImageManipulator("games/".$game_info["uid"]."/500px_thumb.png");
      $w = $i->getWidth();
      $h = $i->getHeight();
      if ($w > $h) {
        $remaining = ($w-$h)/2;
        $i->crop($remaining, 0, $remaining+$h, $h);
      } else {
        $remaining = ($h-$w)/2;
        $i->crop(0, $remaining, $w, $remaining+$w);
      }

      $i->resample(500, 500);

      if (file_exists ("games/".$game_info["uid"]."/500px_thumb.png")) {
        unlink ("games/".$game_info["uid"]."/500px_thumb.png");
      }
      $i->save("games/".$game_info["uid"]."/500px_thumb.png", IMAGETYPE_PNG);

      if (file_exists ("games/".$game_info["uid"]."/500px_thumb_temp.png")) {
        unlink ("games/".$game_info["uid"]."/500px_thumb_temp.png");
      }

      header("Location: game_project.php?game=".$_GET["game"]);
      exit;
    }
  } else {
    $imageUploadError = "Invalid file type.";
    die("Invalid image file type.");
  }
}

?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Edit Game Photo :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
      <h1>
        New Thumbnail <a href="game_project.php?game=<?=$_GET["game"] ?>"><small ><font class="hide-for-small"><?=$game_info["name"] ?></font></small></a>
      </h1>
      <hr>
      <div class="row">
        <div class="twelve columns" style="margin-bottom:30px;">
          Please resize your photo to 500px by 500px before uploading.<br><br>
          <form action="game_edit_photo.php?game=<?=$_GET["game"] ?>" method="post" enctype="multipart/form-data">
            <h5 class="subheader">
              <b>Select an image: <input type="file" name="file" id="file"></b>
              <br>
              <br>
              <input type="submit" name="submit" value="Post New Thumbnail" class="nice blue radius button" style="color:#333;">
            </h5>
          </form>
        </div>
      </div>

      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
