<?php
require_once("class.vtauth.php");

session_start();
ini_set('session.gc_maxlifetime', 86400);
$authentication_failed = false;


if (isset($_POST["username"]) && isset($_POST["password"])) {
    $new_user = new VTAuth($_POST["username"], $_POST["password"]);
    $new_user->pidAuth();
    if ($new_user->latest_status == VTAuth::VTAUTH_GOOD) {
        $_SESSION["user"] = $new_user;
        if (isset($_GET["from"])) {
            Header("Location: ".$_GET["from"]);
            exit;
        } else {
            Header("Location: .");
            exit;
        }
    } else {
        $authentication_failed = true;
        if (isset($_SESSION["user"])) { unset($_SESSION["user"]); }
    }
}

if (isset($_SESSION["user"]) && $_SESSION["user"]->username == "") {
    unset($_SESSION["user"]);
}
