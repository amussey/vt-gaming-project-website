<?php
require_once("plugin.game_info.php");


$onteam = false;
if (isset($_SESSION["user"])) {
    $query = $db->prepare("SELECT * FROM game_team WHERE game_id=:game_id AND pid=:pid LIMIT 1");
    $query->execute(array(
        ":game_id" => $game_info["id"],
        ":pid" => $_SESSION["user"]->username
    ));
    if (count($query->fetchAll(PDO::FETCH_ASSOC)) > 0) {
        $onteam = true;
    }
}

if (!$onteam) {
    header("Location: game_project.php?game=".$_GET["game"]);
    exit;
}
