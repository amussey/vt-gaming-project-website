<?php

define("DEBUG", false, true);
require("inc.mysql.config.php");

// The root for the project on the webserver.
$DIR_root = "/dev";


try {
    $mysql_connection_string = "mysql:host=$mysql_server;port=$mysql_port;dbname=$mysql_database";
    $db = new PDO($mysql_connection_string, $mysql_username, $mysql_password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
} catch (PDOException $err) {
    // Catch Exceptions from the above code for our Exception Handling
    die("There is currently a problem with the MySQL database.  Please try again later.");
}
