<?php

if (isset($_SERVER["HTTP_HOST"]) && strpos($_SERVER["HTTP_HOST"], ".vt.edu") !== false) {
	if (!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on") {
		header("Location: ".str_replace("http://", "https://secure.org.vt.edu/", $_SERVER["SCRIPT_URI"]));
		exit;
	}
}
