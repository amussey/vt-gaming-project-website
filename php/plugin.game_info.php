<?php

$game = $_GET["game"];
$game_id = getGameID($game);
if ($game_id === false) {
    header("Location: games.php");
    exit;
}
$game_info = getGameInfo($game_id);
