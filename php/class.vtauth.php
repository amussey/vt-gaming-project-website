<?php
/**
* Class to auth a pid and password to just check the existance of a pid.
* @author Bryan Clay
* @version 0.81 (11/03/05)
*/
class VTAuth {
	const VTAUTH_GOOD = 0; //good pid and pass
	const VTAUTH_BADPID = -1; //non-existant pid
	const VTAUTH_BADPASS = -2; //pid exists, but bad pass
	const VTAUTH_LDAPERROR = -3; //error searching/connecting to ldap server
	const VTAUTH_BADARG = -4; //bad arguments passed to the method
	

    // assigned a static value using an expression in the constructor
    public $username;
    public $password;
    public $user_id;
    public $latest_status;


    
    public function __construct($usernameValue, $passwordValue)
    {
        // sets to $myStaticVariable (or 'Some Static Variable Value' in this example)
        $this->username = strtolower($usernameValue);
        
        // sets to newly assigned-to $this->myStaticVariable
        $this->password = $passwordValue;
        $this->user_id = NULL;
        $this->latest_status = NULL;
    }
    


	public function pidAuth() {//) {
		$pid = $this->username;
		$credential = $this->password;
		$secure=true;
		$host = "ldap://authn.directory.vt.edu";
		$baseDn = "ou=people,dc=vt,dc=edu";
		$userfield = "uupid";
		
		/*ldap will bind anonymously, make sure we have some credentials*/
		if (!empty($pid)) {
			$ldap = ldap_connect($host);
			ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			if( $secure ) ldap_start_tls($ldap);
			if (isset($ldap) && $ldap != '') {
				/* search for pid dn */
				$result = @ldap_search($ldap, $baseDn, $userfield.'='.$pid, array('dn'));
				if ($result != 0) {
					$entries = ldap_get_entries($ldap, $result);
					$principal = $entries[0]['dn'];
					
					//echo "<a href=\"http://search.vt.edu/search/person.html?person=".$user_id."\">".$user_id."</a>";
					//print_r(str_replace(",ou=People,dc=vt,dc=edu", "", str_replace("uid=", "", $principal)));
					if (isset($principal)) {
						/* bind as this user */
						$this->user_id = str_replace(",ou=People,dc=vt,dc=edu", "", str_replace("uid=", "", $principal));
						if (@ldap_bind($ldap, $principal, $credential)) { 
							//good username and pass
							$this->latest_status = self::VTAUTH_GOOD;
							return self::VTAUTH_GOOD;
						}
						else { 
							// Authenticate failure
							$this->latest_status = self::VTAUTH_BADPASS;
							return self::VTAUTH_BADPASS;
						}
					}
					else {
						// User not found in LDAP
						$this->latest_status = self::VTAUTH_BADPID;
						return self::VTAUTH_BADPID;
					}
					ldap_free_result($result);
				}
				else { 
					// Error occured searching the LDAP
					$this->latest_status = self::VTAUTH_LDAPERROR;
					return self::VTAUTH_LDAPERROR;
				}
				ldap_close($ldap);
			}
			else { 
				// Could not connect to LDAP
				$this->latest_status = self::VTAUTH_LDAPERROR;
				return self::VTAUTH_LDAPERROR;
			}
		}
		else {
			$this->latest_status = self::VTAUTH_BADARG;
			return self::VTAUTH_BADARG;
		}
		
		return NULL; //will never get here, all return paths are already covered
	}
	

	/**
	* Check to see if a pid is valid.
	*/
	public function validatePid() {//$pid = $this->username, $secure=true) {
		$pid = $this->username;
		$result = self::pidAuth(); //$pid, $this->password, $secure);
		if( $result == self::VTAUTH_BADPASS ) return self::VTAUTH_GOOD;
		return $result;
	}
	

	/**
	* returns an english string representing an error code.
	*/
	public function errorToString() {
		$error = $this->latest_status;
		switch($error) {
			case self::VTAUTH_GOOD:
				return "No Error";
			case self::VTAUTH_BADPID:
				return "PID non existant";
			case self::VTAUTH_BADPASS:
				return "Bad Password";
			case self::VTAUTH_LDAPERROR:
				return "Problem searching/connecting to LDAP server";
			case self::VTAUTH_BADARG:
				return "Bad arguments passed to method";
			default:
				return "Unknown error";
		}
	}
}

?>