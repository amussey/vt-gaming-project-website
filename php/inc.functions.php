<?php
require_once("inc.mysql.php");
require_once("plugin.sessions.php");

ini_set('display_errors', 'On');
error_reporting(E_ALL);
ini_set( 'magic_quotes_gpc', 0 );

function square_game($title = "Your Game Here!", $keyword = "placeholder",  $hide_update = false, $last_update = 0) {
    $image = "http://placehold.it/500x500&text=%3F";
    if ($keyword != "placeholder") {
        $image = "games/".$keyword."/500px_thumb.png";
    }
    ?>
          <a href="<?=($keyword != "placeholder" ? "game_project.php?game=".$keyword : "game_new.php" ) ?>">
          <div class="three mobile-two columns">
            <img src="<?=$image ?>">
            <div class="panel">
              <p>
                <?=stripslashes($title) ?><?php
                if (!$hide_update) {
                ?>
                    <br>
                    <font style="font-size:9px;">
                        <b><?=($last_update == 0 ? "&nbsp;" : "Last Update:") ?></b>
                           <?=($last_update == 0 ? ""       : $last_update  ) ?>
                    </font>
                <?php } ?>
              </p>
            </div>
          </div>
          </a>
<?php
}


function getGameID($game) {
    global $db;

    $query = $db->prepare("SELECT id FROM games WHERE uid=:game LIMIT 1");
    $query->execute(array(":game" => $game));
    $game_id = $query->fetchAll(PDO::FETCH_ASSOC);

    if (count($game_id) > 0) {
        return $game_id[0]["id"];
    }
    return false;
}


/**
 * This is a placeholder for the getGameInfo for the moment.
 */
function getGameInfo($game) {
    global $db;
    $game_id = (is_numeric($game) ? $game : getGameID($game));

    // Get the latest update time for the game

    $query = $db->prepare("SELECT * FROM games WHERE id=:game_id LIMIT 1");
    $query->execute(array(":game_id" => $game_id));
    $game_info = $query->fetchAll(PDO::FETCH_ASSOC);

    if (count($game_info) == 0) {
        return false;
    }
    $game_info = $game_info[0];



    $platforms = array();
    $platform_numbers = explode(";", $game_info["platforms"]);
    for ($i = 0; $i < count($platform_numbers); $i++) {
        $query = $db->prepare("SELECT * FROM platforms WHERE id=:platform_id LIMIT 1");
        $query->execute(array(":platform_id" => $platform_numbers[$i]));
        $platform_info = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($platform_info) > 0) {
            array_push($platforms, $platform_info[0]);
        }
    }


    $query = $db->prepare("
        (SELECT started AS `date` FROM games WHERE id=:game_id)
        UNION
        (SELECT `date` FROM game_updates WHERE game_id=:game_id)
        ORDER BY `date` DESC LIMIT 1");
    $query->execute(array(":game_id" => $game_id));
    $latest_update = $query->fetchAll(PDO::FETCH_ASSOC);



    // Fetch the people on the team for the game.
    $query = $db->prepare("SELECT pid, uid, joined FROM game_team WHERE game_id=:game_id ORDER BY joined ASC");
    $query->execute(array(":game_id" => $game_id));
    $team_members = $query->fetchAll(PDO::FETCH_ASSOC);


    //grab the downloads
    $query = $db->prepare("SELECT * FROM game_downloads WHERE  game_id=:game_id ORDER BY filename ASC");
    $query->execute(array(
        ":game_id" => $game_id
    ));
    $downloads = $query->fetchAll(PDO::FETCH_ASSOC);

    for ($dl = 0; $dl < count($downloads); $dl++) {
        if ($downloads[$dl]["platform"] == -1) {
            $downloads[$dl]["platform"] = "";
        } else {
            $query = $db->prepare("SELECT * FROM platforms WHERE  id=:id  LIMIT 1");
            $query->execute(array(
                ":id" => $downloads[$dl]["platform"]
            ));
            $dlplatforms = $query->fetchAll(PDO::FETCH_ASSOC);
            $downloads[$dl]["platform"] = isset($dlplatforms[0]["name"]) ? $dlplatforms[0]["name"] : "";
        }
        if ($downloads[$dl]["filename"] == "") {
            $downloads[$dl]["size"] = "";
            if ($downloads[$dl]["description"] == "") {
                $downloads[$dl]["description"] = end(explode("/", $downloads[$dl]["url"]));
            }
            $downloads[$dl]["link"] = "<a href=\"".$downloads[$dl]["url"]."\">".$downloads[$dl]["description"]."</a>";
            $downloads[$dl]["filename"] = $downloads[$dl]["description"];
        } else {
            $downloads[$dl]["size"] = (intval(($downloads[$dl]["size"] / (1024*1024))*10)/10). " Mb";
            $final_file_name  = "games/".$game_info["uid"]."/downloads/".$downloads[$dl]["filename"];
            $downloads[$dl]["link"] = "<a href=\"".$final_file_name."\">".$downloads[$dl]["filename"]."</a>";
        }
    }

    $updates = get_updates($game_id, 1);


    return array(
      "id"          => $game_id,
      "name"        => $game_info["name"],
      "uid"         => $game_info["uid"],
      "keywords"    => $game_info["keywords"],
      "description" => $game_info["description"],
      "platforms"   => $platforms,
      "repo"        => $game_info["repo"],
      "moddb"       => $game_info["moddb"],
      "downloads"   => $downloads,
      "updates"     => $updates,
      "last_update" => date("Y", strtotime($latest_update[0]["date"])),
      "team"        => $team_members
    );
}


function get_updates($game_id, $number_of_updates = 0) {
    global $db;
    $query = $db->prepare("SELECT game_id, pid, message, major, unix_timestamp(date) AS date, unix_timestamp(edit_date) AS edit_date FROM game_updates WHERE game_id=:game_id ORDER BY date DESC ".
        (($number_of_updates != 0) ? " LIMIT ".$number_of_updates : ""));
    $query->execute(array(
        ":game_id" => $game_id
    ));
    return $query->fetchAll(PDO::FETCH_ASSOC);
}
