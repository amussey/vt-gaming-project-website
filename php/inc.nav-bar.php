<?php
require_once("inc.mysql.php");
require_once("plugin.sessions.php");

?>
  <div class="row">
    <div class="three columns">
      <h1><a href="."><img src="img/top_banner_transparent.png" /></a></h1>
    </div>

    <div class="nine columns">
      <ul class="nav-bar right">
      <li <?=($page == "index" ? "class=\"active\"" : "") ?>><a href="index.php">Home</a></li>
      <li <?=($page == "games" ? "class=\"active\"" : "") ?>><a href="games.php">Games</a></li>
      <li <?=($page == "sponsored" ? "class=\"active\"" : "") ?>><a href="game_project.php?game=sponsored">Sponsored</a></li>
      <li><a href="https://www.facebook.com/groups/675861402438724/events/">Events</a></li>
      <?php
      if (isset($_SESSION["user"])) { ?>
        <li><a href="#">Hi <?=$_SESSION["user"]->username ?>!</a></li>
        <?php
        $query = $db->prepare("SELECT * FROM  `admins` WHERE pid=:pid LIMIT 1");
        $query->execute(array(":pid" => $_SESSION["user"]->username));
        $user_data = $query->fetchAll(PDO::FETCH_ASSOC);

        // Display the admin panel at the top of the page.  Depricated.  Remove "false" flag to enable.
        if (count($user_data) > 0 && false) { ?>
          <li><a href="admin/">Admin</a></li>
        <?php } ?>
        <li><a href="logout.php">Logout</a></li>
        <?php
      } else {
        ?><li><a href="login.php">Login</a></li><?php
      } ?>
      </ul>
    </div>
  </div>
