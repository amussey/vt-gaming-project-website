<?php
//game_project.php
require_once("php/inc.functions.php");
require_once("php/plugin.game_info.php");
require_once("php/plugin.game_req_on_team.php");


if (isset($_GET["screenshot_id"])) {
  $query = $db->prepare("SELECT * FROM game_screenshots WHERE id=:id LIMIT 1");
  $query->execute(array(
    ":id" => $_GET["screenshot_id"]
  ));
  $screenshot_info = $query->fetchAll(PDO::FETCH_ASSOC);
  $screenshot_info = $screenshot_info[0];

  $query = $db->prepare("DELETE FROM game_screenshots WHERE id=:id");
  $query->execute(array(
    ":id" => $_GET["screenshot_id"]
  ));

  $final_file_name  = "games/".$game_info["uid"]."/screenshots/".$screenshot_info["id"].".".$screenshot_info["format"];
  $final_thumb_name = "games/".$game_info["uid"]."/screenshots/".$screenshot_info["id"].".thumb.jpg";
  unlink($final_file_name);
  unlink($final_thumb_name);
}

$query = $db->prepare("
  SELECT *
  FROM  `game_screenshots`
  ORDER BY  `game_screenshots`.`upload` DESC");
$query->execute(array());
$screenshots = $query->fetchAll(PDO::FETCH_ASSOC);


?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Delete Screenshots :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
      <h1>
        Delete Screenshots 
        <a href="game_project.php?game=<?=$game ?>"><small ><font class="hide-for-small">Click to delete</font></small></a>
        <a href="game_screenshots.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:32px;">Done</small></a>
      </h1>
      <hr>

      <div class="row">
        <?php
        for ($i = 0; $i < count($screenshots); $i++) { ?>
          <a href="game_screenshots_delete.php?game=<?=$game_info["uid"] ?>&screenshot_id=<?=$screenshots[$i]["id"] ?>"><div class="three mobile-two columns" style="margin-bottom:30px;"><img src="games/<?=$game_info["uid"] ?>/screenshots/<?=$screenshots[$i]["id"] ?>.thumb.jpg"></div></a>
          <?php
        }
        for ($i = 0; count($screenshots) % 4 != 0 && $i < 4 - (count($screenshots) % 4); $i++) { ?>
          <div class="three mobile-two columns" style="margin-bottom:30px;"></div>
          <?php
        } ?>
      </div>

      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script></body>
</html>
