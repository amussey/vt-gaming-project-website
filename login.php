<?php
require_once("php/plugin.force_ssl.php");
require_once("php/plugin.sessions.php");


if (isset($_SESSION["user"])) {
    header("Location: .");
}

?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>Login :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="four columns"></div>
    <div class="four columns">
      <h1><a href="."><img src="img/top_banner_transparent.png" /></a></h1>
    </div>
    <div class="four columns"></div>
  </div>

  <div class="row">
    <div class="four columns"></div>
    <div class="panel four columns">
      <form action="login.php?<?=urlencode($_SERVER["QUERY_STRING"]) ?>" method="POST">
        <h3>Login  <small><?=($authentication_failed ? "Incorrect login.  Try again." : "Use your VT PID.") ?></small><hr></h3>

        <input type="text" placeholder="Username" name="username" id="username">
        <input type="password" placeholder="Password" name="password" id="password"><br>
        <input type="submit" class="button right" value="Login" />
      </form>
    </div>
    <div class="four columns"></div>
  </div>

  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
