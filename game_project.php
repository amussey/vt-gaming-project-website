<?php
require_once("php/inc.functions.php");
require_once("php/inc.mysql.php");
require_once("php/plugin.sessions.php");
require_once("php/plugin.game_info.php");


if (isset($_GET["join_team"])) {
  if (isset($_SESSION["user"])) {
    $query = $db->prepare("SELECT * FROM game_team WHERE game_id=:game_id AND pid=:pid LIMIT 1");
    $query->execute(array(
      ":game_id" => $game_info["id"],
      ":pid" => $_SESSION["user"]->username
    ));
    if (count($query->fetchAll(PDO::FETCH_ASSOC)) == 0) {
      $query = $db->prepare("INSERT INTO game_team (game_id, pid, uid, joined) VALUES (:game_id, :pid, :uid, FROM_UNIXTIME(".time()."))");
      $query->execute(array(
        ":game_id" => $game_info["id"],
        ":pid" => $_SESSION["user"]->username,
        ":uid" => $_SESSION["user"]->user_id
      ));
      $game_info = getGameInfo($_GET["game"]);
    }
  }
}

if (isset($_GET["leave_team"])) {
  if (isset($_SESSION["user"])) {
    $query = $db->prepare("DELETE FROM game_team WHERE game_id=:game_id AND pid=:pid");
    $query->execute(array(
      ":game_id" => $game_info["id"],
      ":pid" => $_SESSION["user"]->username
    ));
    $game_info = getGameInfo($_GET["game"]);
  }
}

$onteam = false;
if (isset($_SESSION["user"])) {
  $query = $db->prepare("SELECT * FROM game_team WHERE game_id=:game_id AND pid=:pid LIMIT 1");
  $query->execute(array(
    ":game_id" => $game_info["id"],
    ":pid" => $_SESSION["user"]->username
  ));
  if (count($query->fetchAll(PDO::FETCH_ASSOC)) > 0) {
    $onteam = true;
  }
}


?><!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title><?=stripslashes($game_info["name"]) ?> :: VT Gaming Project</title>

  <!-- Included CSS Files -->
  <link rel="stylesheet" href="css/foundation.min.css">
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="row">
    <div class="twelve columns">
      <?php $page = "games"; include("php/inc.nav-bar.php"); ?>
      <h1>
      <?=stripslashes($game_info["name"]) ?> <small ><font class="hide-for-small"></font></small>
      <?php
      if (isset($_SESSION["user"])) {
        if ($onteam) { ?>
          <a href="game_project.php?game=<?=$_GET["game"] ?>&leave_team"><small style="float:right; margin-top:32px;">Leave Team</small></a>
          <small style="float:right; margin-top:32px;"> &nbsp;/&nbsp; </small>
          <a href="game_edit_photo.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:32px;">Photo</small></a>
          <small style="float:right; margin-top:32px;"> &nbsp;/&nbsp; </small>
          <a href="game_edit.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:32px;">Edit Info</small></a>
          <?php
        } else {
          ?>
          <a href="game_project.php?game=<?=$_GET["game"] ?>&join_team"><small style="float:right; margin-top:32px;">Join Team</small></a>
          <?php
        }
      }
      ?>
      </h1>
      <hr>

      <div class="row">
        <div class="six columns">
          <img src="games/<?=$game_info["uid"] ?>/500px_thumb.png?time=<?=time() ?>"><!-- http://placehold.it/500x500&text=Image">--><br>
        </div>

        <div class="six columns">
          <div class="panel show-for-small" >
            <h5 class="subheader">
              <?=(strlen($game_info["description"]) > 0 ? $game_info["description"] : "No description.") ?>
              <br><br>
              <b>Platforms</b>: <?php
                
              $platforms = array();
              for ($i = 0; $i < count($game_info["platforms"]); $i++) {
                array_push($platforms, $game_info["platforms"][$i]["name"]);
              }

              echo implode(", ", $platforms) ?><br>
            </h5>
          </div>

          <div class="panel hide-for-small" style="min-height:440px; background-color:transparent; border-color:transparent;">
            <h5 class="subheader">
              <?=(strlen($game_info["description"]) > 0 ? stripslashes($game_info["description"]) : "No description.") ?>
              <br><br>
              <b>Platforms</b>: <?=implode(", ", $platforms) ?><br>
              <?php if (isset($game_info["repo"]) && $game_info["repo"] != "") { ?><b>Source Repository</b>:  <a href="<?=$game_info["repo"] ?>" target="_blank">Click here</a><br><?php } ?>
              <?php if (isset($game_info["moddb"]) && $game_info["moddb"] != "") { ?><b>ModDB Page</b>:  <a href="<?=$game_info["moddb"] ?>" target="_blank">Click here</a><br><?php } ?>
              <b>Team Members</b>:  <?=count($game_info["team"]); ?><br>
              <?php
              if (isset($_SESSION["user"])) {
                if (count($game_info["team"]) > 0) { ?>
                  <b>Team Contacts</b>: <?php
                  for ($i = 0; $i < count($game_info["team"]); $i++) {
                    echo "<a href=\"http://search.vt.edu/search/person.html?person=".$game_info["team"][$i]["uid"]."\">".$game_info["team"][$i]["pid"]."</a>".($i+1 == count($game_info["team"]) ? "<br>" : ", ");
                  }
                }
              }
              ?>
            </h5>
          </div>
        </div>
      </div>

      <br>

      <?php
      // Lets grab some screenshots!
      $query = $db->prepare("
        SELECT *
        FROM  `game_screenshots`
        WHERE game_id=:game_id
        ORDER BY `game_screenshots`.`upload` DESC
        LIMIT 4");
      $query->execute(array(":game_id" => $game_info["id"]));
      $screenshots = $query->fetchAll(PDO::FETCH_ASSOC);

      ?>

      <h3 class="hide-for-small">
        Screenshots <a href="game_screenshots.php?game=<?=$game_info["uid"] ?>"><small ><font class="hide-for-small">Click here for more</font></small></a>
        <?php
        if (isset($_SESSION["user"])) {
          if ($onteam) { ?>
            <a href="game_screenshots_delete.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:19px;" class="hide-for-small">Delete</small></a>
            <small style="float:right; margin-top:19px;" class="hide-for-small"> &nbsp; / &nbsp; </small>
            <a href="game_screenshots_new.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:19px;" class="hide-for-small">Add new</small></a>
            <?php
          }
        }
        ?>
      </h3>
      <hr class="hide-for-small" />

      <!-- Screenshots -->

      <?
      if (count($screenshots) > 0) { ?>
        <div class="row">
          <?php
          for ($i = 0; $i < count($screenshots); $i++) { ?>
            <a href="game_screenshot.php?game=<?=$game_info["uid"] ?>&screenshot_id=<?=$screenshots[$i]["id"] ?>"><div class="three mobile-two columns" style="margin-bottom:30px;"><img src="games/<?=$game_info["uid"] ?>/screenshots/<?=$screenshots[$i]["id"] ?>.thumb.jpg"></div></a>
            <?php
          }
          for ($i = 0; $i < (4-count($screenshots)); $i++) { ?>
            <div class="three mobile-two columns hide-for-small"></div>
            <?php
          } ?>
        </div>
      <?php
      }
      ?>

      <!-- End Screenshots -->


      <a href="game_updates.php?game=<?=$game_info["uid"] ?>"><h3>Updates <small ><font class="hide-for-small">Click here for more </font></small></h3></a>
      <hr>

      <!-- Updates -->
      <div class="row">
        <div class="twelve columns">
          <?php
          if (count($game_info["updates"]) == 0) { ?>
            <center>There are currently no updates for this game.</center><?php
          } else {
            ?>
            <small><?=date("r", $game_info["updates"][0]["date"]) ?><?php
            if (isset($_SESSION["user"])) {
              echo " by ".$game_info["updates"][0]["pid"];
            } ?>
            </small>
            <br>
            <div style="margin-left:10px; margin-top:10px;">
              <?=$game_info["updates"][0]["message"] ?>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
      <br><br>

      <!-- End Updates -->

      <h3 class="hide-for-small">Downloads <small ><font class="hide-for-small">Binaries? Source? Fun!</font></small>
      <?php
      if (isset($_SESSION["user"])) {
        if ($onteam) { ?>
          <a href="game_downloads_delete.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:19px;" class="hide-for-small">Delete</small></a>
          <small style="float:right; margin-top:19px;" class="hide-for-small"> &nbsp; / &nbsp; </small>
          <a href="game_downloads_new.php?game=<?=$_GET["game"] ?>"><small style="float:right; margin-top:19px;" class="hide-for-small">Add new</small></a>
          <?php
        }
      } ?>
      </h3>
      <hr class="hide-for-small" />

      <!-- Downloads -->
      <?php
      for ($i = 0; $i < count($game_info["downloads"]); $i++) { ?>
        <div class="row hide-for-small">
          <div class="seven columns"><?=$game_info["downloads"][$i]["link"] ?></div>
          <div class="three columns"><?=$game_info["downloads"][$i]["platform"] ?></div>
          <div class="two columns"><?=$game_info["downloads"][$i]["size"] ?></div>
        </div>
        <br class="hide-for-small" />
        <?php
      }
      ?>
      <!-- End Downloads -->

      <!-- Footer -->
      <?php include "php/inc.footer.php"; ?>
      <!-- End Footer -->
    </div>
  </div>
 
  <!-- Included JS -->
  <script src="js/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
